import { ProductService } from './../product.service';
import { Product } from './../product.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {
  product: Product = {
    name: "",
    price: null

  };
  constructor(private productService: ProductService,
    private activatedRoute: ActivatedRoute, private route: Router) { 
      
    }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.productService.readById(id!).subscribe(product=>{
      this.product=product;
    });
  }

  cancel(): void {
    this.route.navigate(["/products"]);
  }

  updateProduct(): void {
    this.productService.update(this.product).subscribe(()=>{
       this.route.navigate(["/products"]);
    });
  }

}
